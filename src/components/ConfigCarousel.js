import React from 'react';
import { Carousel, CarouselCaption, CarouselInner, CarouselItem, View, MDBBtn } from "mdbreact";


function OneSlide(props) {
    let image = require("../assets/configurateur/"+props.path+"/"+props.slide.img);

    return (
        <CarouselItem itemId={props.index+1}>
            <View>
                <img className="d-block w-100 rounded mx-auto d-block" src={image} alt={props.slide.title} />       
            </View>
            <CarouselCaption>
                <h3 className="h3-responsive">{props.slide.title}</h3>
                <p>{props.slide.price}</p>
                {(typeof props.withButton === "undefined" || props.withButton === true) && 
                    <MDBBtn onClick={props.slide.onclick} color="blue-grey">Selectioner</MDBBtn>
                }
            </CarouselCaption>
        </CarouselItem>
    )
}

export default function ConfigCarousel(props) {
    return (
        <Carousel activeItem={1} length={props.slides.length} showControls={true} showIndicators={true} className="z-depth-1">
            <CarouselInner>

                { props.slides.map((slide, i) => {
                    return <OneSlide path={props.path} withButton={props.withButton} slide={slide} key={i} index={i} />
                }) }

            </CarouselInner>
        </Carousel>
    );
}