import React, { Component } from 'react';
import { Carousel, CarouselCaption, CarouselInner, CarouselItem, View, MDBBtn, MDBMask, MDBView, MDBContainer, MDBRow, MDBCol, Input} from "mdbreact";
import './App.css';

import ConfigCarousel from './components/ConfigCarousel';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      version:"",
      color:"",
      jante:"",
      interieur: "",
      selectedProducts: {
        modele: [
          {id: 1, price: 99.5}
        ],
        couleur: [],
        jante: [],
        interieur: [],
        accessoires: [],
        equipements: []
      }
    };
    this.versionSelectedPure = this
        .versionSelectedPure
        .bind(this);
    this.versionSelectedLegende = this
        .versionSelectedLegende
        .bind(this);
    this.colorSelectedPureWhite = this
        .colorSelectedPureWhite
        .bind(this);
    this.colorSelectedPureBlue = this
        .colorSelectedPureBlue
        .bind(this);
    this.colorSelectedPureBlack = this
        .colorSelectedPureBlack
        .bind(this);
    this.colorSelectedLegendeWhite = this
        .colorSelectedLegendeWhite
        .bind(this);
    this.colorSelectedLegendeBlue = this
        .colorSelectedLegendeBlue
        .bind(this);
    this.colorSelectedLegendeBlack = this
        .colorSelectedLegendeBlack
        .bind(this);
    this.janteSelectedPureStandard = this
        .janteSelectedPureStandard
        .bind(this);
    this.janteSelectedPureSerac = this
        .janteSelectedPureSerac
        .bind(this);
    this.janteSelectedLegende = this
        .janteSelectedLegende
        .bind(this);
    this.interieurSelectedPureNoirD = this
        .interieurSelectedPureNoirD
        .bind(this);  
    this.interieurSelectedPureNoirP = this
        .interieurSelectedPureNoirP
        .bind(this);
    this.interieurSelectedLegendeBrun = this
        .interieurSelectedLegendeBrun
        .bind(this);
    this.interieurSelectedLegendeNoir = this
        .interieurSelectedLegendeNoir
        .bind(this);
        

  }

  versionSelectedPure(){
    // document.querySelector('#modelePure').classList.add('selected')
    // document.querySelector('#modeleLegende').classList.remove('selected')

    this.setState(state => {
      state.selectedProducts["modele"] = [
        {id: 1, price: 999}
      ];

      return {
        version: "Pure", color: "White", jante: "Standard", interieur: "NoirD"
      }
    })

  }

  versionSelectedLegende(){
    
    this.setState(state => {
      state.selectedProducts["modele"] = [
        {id: 2, price: 666}
      ];
      return {version: "Legende", color: "White", interieur: "Brun"}
    });
  }

  colorSelectedPureWhite(){
    this.setState(state => ({color: "White"}))
  }  

  colorSelectedPureBlue(){
    this.setState(state => ({color: "Blue"}))
  }

  colorSelectedPureBlack(){
    this.setState(state => ({color: "Black"}))
  }

  colorSelectedLegendeWhite(){
    this.setState(state => ({color: "White"}))
  } 

  colorSelectedLegendeBlue(){
    this.setState(state => ({color: "Blue"}))
  }

  colorSelectedLegendeBlack(){
    this.setState(state => ({color: "Black"}))
  }

  janteSelectedPureStandard(){
    this.setState(state => ({jante: "Standard"}))
  }

  janteSelectedPureSerac(){
    this.setState(state => ({jante: "Serac"}))
  }

  janteSelectedLegende(){
    this.setState(state => ({jante: "legende"}))
  }

  interieurSelectedPureNoirD(){
    this.setState(state => ({interieur: "NoirD"}))
  }

  interieurSelectedPureNoirP(){
    this.setState(state => ({interieur: "NoirP"}))
  }

  interieurSelectedLegendeBrun(){
    this.setState(state => ({interieur: "Brun"}))
  }

  interieurSelectedLegendeNoir(){
    this.setState(state => ({interieur: "Noir"}))
  }

  render() {
    return (
      <div className="App">
        {this.state.version === "" &&
          <ConfigCarousel path="modele/selection" slides={[
              {
                img: "legende.png",
                title: "Legende",
                price: "58 500€",
                onclick: this.versionSelectedLegende
              },
              {
                img: "pure.png",
                title: "Pure",
                price: "54 700€",
                onclick: this.versionSelectedPure
              }
          ]} />
        }

        {/* choix de la version Pure blanche jante standard */}
        {this.state.version === "Pure" &&
          <div>
            {(this.state.color === "White" && this.state.jante === "Standard")&& 
              <ConfigCarousel path="modele/pure" slides={[
                {
                  img: "modele_pure-couleur_blanche-jante_standard (1).jpg",
                  title: "Pure blanche",
                  price: "54 700€"
                },
                {
                  img: "modele_pure-couleur_blanche-jante_standard (2).jpg",
                  title: "Pure blanche",
                  price: "54 700€"
                },
                {
                  img: "modele_pure-couleur_blanche-jante_standard (3).jpg",
                  title: "Pure blanche",
                  price: "54 700€"
                },
                {
                  img: "modele_pure-couleur_blanche-jante_standard (4).jpg",
                  title: "Pure blanche",
                  price: "54 700€"
                }
            ]} />

            }

            {(this.state.color === "White" && this.state.jante === "Serac")&& 
            <ConfigCarousel path="modele/pure" slides={[
              {
                img: "modele_pure-couleur_blanche-jante_serac (1).jpg",
                title: "Pure blanche",
                price: "55 700€"
              },
              {
                img: "modele_pure-couleur_blanche-jante_serac (2).jpg",
                title: "Pure blanche",
                price: "55 700€"
              },
              {
                img: "modele_pure-couleur_blanche-jante_serac (3).jpg",
                title: "Pure blanche",
                price: "55 700€"
              },
              {
                img: "modele_pure-couleur_blanche-jante_serac (4).jpg",
                title: "Pure blanche",
                price: "55 700€"
              }
          ]} />

          }



            {/* choix Pure bleu */}
            {(this.state.color === "Blue" && this.state.jante === "Standard")&&
              <ConfigCarousel path="modele/pure" slides={[
                {
                  img: "modele_pure-couleur_bleu-jante_standard (1).jpg",
                  title: "Pure bleu",
                  price: "56 500€"
                },
                {
                  img: "modele_pure-couleur_bleu-jante_standard (2).jpg",
                  title: "Pure bleu",
                  price: "56 500€"
                },
                {
                  img: "modele_pure-couleur_bleu-jante_standard (3).jpg",
                  title: "Pure bleu",
                  price: "56 500€"
                },
                {
                  img: "modele_pure-couleur_bleu-jante_standard (4).jpg",
                  title: "Pure bleu",
                  price: "56 500€"
                },
                
            ]} />
              
            }

          {/* choix Pure bleu */}
          {(this.state.color === "Blue" && this.state.jante === "Serac")&&
            <ConfigCarousel path="modele/pure" slides={[
              {
                img: "modele_pure-couleur_bleu-jante_serac (1).jpg",
                title: "Pure bleu",
                price: "57 500€"
              },
              {
                img: "modele_pure-couleur_bleu-jante_serac (2).jpg",
                title: "Pure bleu",
                price: "57 500€"
              },
              {
                img: "modele_pure-couleur_bleu-jante_serac (3).jpg",
                title: "Pure bleu",
                price: "57 500€"
              },
              {
                img: "modele_pure-couleur_bleu-jante_serac (4).jpg",
                title: "Pure bleu",
                price: "57 500€"
              },
              
          ]} />
            
          }




            {/* choix Pure noire */}
            {(this.state.color === "Black" && this.state.jante === "Standard")&& 
            <ConfigCarousel path="modele/pure" slides={[
              {
                img: "modele_pure-couleur_noire-jante_standard (1).jpg",
                title: "Pure noire",
                price: "55 540€"
              },
              {
                img: "modele_pure-couleur_noire-jante_standard (2).jpg",
                title: "Pure noire",
                price: "55 540€"
              },
              {
                img: "modele_pure-couleur_noire-jante_standard (3).jpg",
                title: "Pure noire",
                price: "55 540€"
              },
              {
                img: "modele_pure-couleur_noire-jante_standard (4).jpg",
                title: "Pure noire",
                price: "55 540€"
              }
            ]} />
              
          }


          {/* choix Pure noire */}
          {(this.state.color === "Black" && this.state.jante === "Serac")&& 
          <ConfigCarousel path="modele/pure" slides={[
            {
              img: "modele_pure-couleur_noire-jante_serac (1).jpg",
              title: "Pure noire",
              price: "56 540€"
            },
            {
              img: "modele_pure-couleur_noire-jante_serac (2).jpg",
              title: "Pure noire",
              price: "56 540€"
            },
            {
              img: "modele_pure-couleur_noire-jante_serac (3).jpg",
              title: "Pure noire",
              price: "56 540€"
            },
            {
              img: "modele_pure-couleur_noire-jante_serac (4).jpg",
              title: "Pure noire",
              price: "56 540€"
            }
          ]} />           
          }

            {/* // carousel pour choix de la couleur */}
            
            <div>
              <MDBContainer  className="version">
                <MDBRow>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/blanc.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedPureWhite}/>
                      <p id="titre">Teinte spéciale blanc Glacier</p>
                      <p>0€</p>
                      <MDBBtn onClick={this.colorSelectedPureWhite} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src= {require("./assets/configurateur/couleurs/selection/bleu.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedPureBlue}/>
                      <p id="titre">Teinte spéciale Bleu Alpine</p>
                      <p>1 800€</p>
                      <MDBBtn onClick={this.colorSelectedPureBlue} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src= {require("./assets/configurateur/couleurs/selection/noir.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedPureBlack}/>
                      <p id="titre">Teinte métallisée Noir Profont</p>
                      <p>840€</p>
                      <MDBBtn onClick={this.colorSelectedPureBlack} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </div>


 {/* choix des jantes</MDBBtn> */}

          <div className="text-center">
          <h2>choix des jantes</h2>
          </div>    
           
   
          {(this.state.jante === "" || this.state.jante === "Standard")  &&
            <div>
              
              <MDBContainer className="jante">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3">              
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-standard.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.janteSelectedPureStandard} />
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedPureStandard} size="sm">Jante standard</MDBBtn>
                    </MDBView>
                       <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-serac.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.janteSelectedPureSerac}/>
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedPureSerac} size="sm">Jante serac</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  
                  {this.state.color === "White" &&     
                  <MDBCol lg="9" className="mb-8 jante">
                    <img src={require("./assets/configurateur/jantes/vues/couleur-blanc_jante-standard (2).jpg")} className="img-fluid"
                        alt=""/>
                  </MDBCol>
                  }

                  {this.state.color ==="Blue" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-bleu_jante-standard (3).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>
                  }

                  {this.state.color ==="Black" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-noir_jante-standard (1).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>                 
                   }                              
                </MDBRow>
              </MDBContainer>           
            </div> 
            }



            {(this.state.jante === "" || this.state.jante === "Serac")  &&
            <div>
              
              <MDBContainer className="jante">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3">              
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-standard.jpg")} className="img-fluid choixJ"
                          alt=""/>
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedPureStandard} size="sm">Jante standard</MDBBtn>
                      <p>0€</p>
                    </MDBView>
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-serac.jpg")} className="img-fluid choixJ"
                          alt=""/>
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedPureSerac} size="sm">Jante serac</MDBBtn>
                      <p>1000€</p>
                    </MDBView>
                  </MDBCol>
                  
                  {this.state.color === "White" &&     
                  <MDBCol lg="9" className="mb-8 jante">
                    <img src={require("./assets/configurateur/jantes/vues/couleur-blanc_jante-serac (2).jpg")} className="img-fluid"
                        alt=""/>
                  </MDBCol>
                  }

                  {this.state.color ==="Blue" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-bleu_jante-serac (3).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>
                  }

                  {this.state.color ==="Black" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-noir_jante-serac (1).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>                 
                   }                              
                </MDBRow>
              </MDBContainer>           
            </div> 
            }


{/* choix de l'interieur */}
          <div className="text-center">
          <h2>Choix de l'intérieur</h2>
          </div>  

          {(this.state.interieur === "" || this.state.interieur === "NoirD") &&
          
              <MDBContainer className="interieur">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3 int">              
                       <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-noir_dinamica.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedPureNoirD}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedPureNoirD} size="sm">Cuir noir dinamica</MDBBtn>
                      <p>0€</p>
                    </MDBView>
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-noir_perfore.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedPureNoirP}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedPureNoirP} size="sm">Cuir noir perforé</MDBBtn>
                      <p>800€</p>
                    </MDBView>
                  </MDBCol>


                  <MDBCol lg="9" className="mb-8 int1">
                  <ConfigCarousel withButton={false} path="interieurs/vues" slides={[
                    {
                      img: "cuir-noir_dinamica-1.jpg",
                    },
                    {
                      img: "cuir-noir_dinamica-2.jpg",
                    },
                    {
                      img: "cuir-noir_dinamica-3.jpg",
                    }
                  ]} />   
                  </MDBCol>

                </MDBRow>
              </MDBContainer>           
            
          }



          {(this.state.interieur === ""  || this.state.interieur === "NoirP") &&
          
              <MDBContainer className="interieur">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3 int">              
                       <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-noir_dinamica.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedPureNoirD}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedPureNoirD} size="sm">Cuir noir dinamica</MDBBtn>
                      <p>0€</p>
                    </MDBView>
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-noir_perfore.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedPureNoirP}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedPureNoirP} size="sm">Cuir noir perforé</MDBBtn>
                      <p>800€</p>
                    </MDBView>
                  </MDBCol>


                  <MDBCol lg="9" className="mb-8 int1">
                  <ConfigCarousel withButton={false} path="interieurs/vues" slides={[
                    {
                      img: "cuir-noir_perfore-1.jpg",
                    },
                    {
                      img: "cuir-noir_perfore-2.jpg",
                    },
                    {
                      img: "cuir-noir_perfore-3.jpg",
                    }
                  ]} />   
                  </MDBCol>

                </MDBRow>
              </MDBContainer>           
             
          }

{/* choix des équipements */}
          <div>
            <h2 className="">Choix des équipements</h2>
          </div>
          <MDBContainer className="equipement">
            <h3>Design</h3>
            <MDBRow>
              <MDBCol lg="2" md="6" >
                <img src={require('./assets/configurateur/equipements/categories/design/pack-heritage.jpg')} className='img-fluid img-thumbnail '/>
                <p>Pack héritage</p>
                <p>180€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2" md="6">
              <img src={require('./assets/configurateur/equipements/categories/design/repose-pied-alu.jpg')} className='img-fluid img-thumbnail'/>
                <p>Repose-pieds passager en aluminium</p>
                <p>96€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
            </MDBRow>
            <h3>Média et navigation</h3>
            <MDBRow>
              <MDBCol lg="2" md="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/alpine-metrics.jpg')} className='img-fluid img-thumbnail'/>
                <p>Alpine Télémétrics</p>
                <p>204€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/audio-focal.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système Audio Focal</p>
                <p>600€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/audio-premium.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système Audio Focal Premium</p>
                <p>1200€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/audio-standard.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système Audio standard</p>
                <p>0€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
            </MDBRow>
            <h3>Confort</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/pack-rangement.jpg')} className='img-fluid img-thumbnail'/>
                <p>Pack de rangement</p>
                <p>504€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/regul-limit-vitesse.jpg')} className='img-fluid img-thumbnail'/>
                <p>Régulateur / limiteur de vitesse</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/retro-ext-chaffant.jpg')} className='img-fluid img-thumbnail'/>
                <p>Rétroviseurs ext. chauf. rabattables élec.</p>
                <p>504€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/retro-int-electrochrome.jpg')} className='img-fluid img-thumbnail'/>
                <p>Retroviseur intérieur électrochrome</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Conduite</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/aide-stationnement-ar.jpg')} className='img-fluid img-thumbnail'/>
                <p>Aide au stationnement AR</p>
                <p>420€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/aide-stationnement-av-ar.jpg')} className='img-fluid img-thumbnail'/>
                <p>Aide au stationnement AV et AR</p>
                <p>750€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/camera-recul.jpg')} className='img-fluid img-thumbnail'/>
                <p>Aide au stationnement AV AR et caméra de recul</p>
                <p>1200€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/echappement-sport.jpg')} className='img-fluid img-thumbnail'/>
                <p>Echappement Sport actif</p>
                <p>1500€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Sécurité</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/securite/aide-freinage-durgence.jpg')} className='img-fluid img-thumbnail'/>
                <p>Assistance au freinage d'urgence</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/securite/airbag.jpg')} className='img-fluid img-thumbnail'/>
                <p>Airbags frontaux conducteur et passager</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/securite/freinage-haute-perf.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système de freinage Haute-Perf 320mm</p>
                <p>1000€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Perso. extérieur</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation exterieure/etrier-bleu.jpg')} className='img-fluid img-thumbnail'/>
                <p>Etriers de frein couleur Bleu Alpine</p>
                <p>384€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation exterieure/etrier-gris.jpg')} className='img-fluid img-thumbnail'/>
                <p>Etriers de frein couleur Gris Anthracite</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation exterieure/logo-alpine.jpg')} className='img-fluid img-thumbnail'/>
                <p>Logo Alpine sur les ailes avant</p>
                <p>120€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Perso. intérieur</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/logo-volant.jpg')} className='img-fluid img-thumbnail'/>
                <p>Logo au centre du volant en Bleu Alpine</p>
                <p>84€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/pack-carbone.jpg')} className='img-fluid img-thumbnail'/>
                <p>Harmonie carbone mat</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/pedal-alu.jpg')} className='img-fluid img-thumbnail'/>
                <p>Pédalier en aluminium</p>
                <p>120€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/siege-chauffant.jpg')} className='img-fluid img-thumbnail'/>
                <p>Sièges chauffants</p>
                <p>400€</p>
                <input filled type="checkbox"/>
              </MDBCol> 
            </MDBRow>

          </MDBContainer>







          </div>
        }









        {/* choix de la version Legende blanche jante standard */}
        {this.state.version === "Legende" &&
          <div>
            {this.state.color === "White" &&
            <ConfigCarousel path="modele/legende" slides={[
              {
                img: "modele_legende-couleur_blanc-jante_legende-1.jpg",
                title: "Legende blanche",
                price: "58 500€"
              },
              {
                img: "modele_legende-couleur_blanc-jante_legende-2.jpg",
                title: "Legende blanche",
                price: "58 500€"
              },
              {
                img: "modele_legende-couleur_blanc-jante_legende-3.jpg",
                title: "Legende blanche",
                price: "58 500€"
              },
              {
                img: "modele_legende-couleur_blanc-jante_legende-4.jpg",
                title: "Legende blanche",
                price: "58 500€"
              },

          ]} />

            }

            {/* choix Legende bleu */}
            {this.state.color === "Blue" &&
            <ConfigCarousel path="modele/legende" slides={[
              {
                img: "modele_legende-couleur_bleu-jante_legende-1.jpg",
                title: "Legende bleu",
                price: "60 300€"
              },
              {
                img: "modele_legende-couleur_bleu-jante_legende-2.jpg",
                title: "Legende bleu",
                price: "60 300€"
              },
              {
                img: "modele_legende-couleur_bleu-jante_legende-3.jpg",
                title: "Legende bleu",
                price: "60 300€"
              },
              {
                img: "modele_legende-couleur_bleu-jante_legende-4.jpg",
                title: "Legende bleu",
                price: "60 300€"
              },

          ]} />
            
          }

            {/* choix Legende noire */}
            {this.state.color === "Black" &&
            <ConfigCarousel path="modele/legende" slides={[
              {
                img: "modele_legende-couleur_noir-jante_legende-1.jpg",
                title: "Legende noire",
                price: "59 340€"
              },
              {
                img: "modele_legende-couleur_noir-jante_legende-2.jpg",
                title: "Legende noire",
                price: "59 340€"
              },
              {
                img: "modele_legende-couleur_noir-jante_legende-3.jpg",
                title: "Legende noire",
                price: "59 340€"
              },
              {
                img: "modele_legende-couleur_noir-jante_legende-4.jpg",
                title: "Legende noire",
                price: "59 340€"
              },

          ]} />
            }
      
            {/* // carousel pour choix de la couleur */}
            <div>
              <MDBContainer  className="version">
                <MDBRow>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/blanc.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedLegendeWhite}/>
                      <p id="titre">Teinte spéciale blanc Glacier</p>
                      <p>0€</p>
                      <MDBBtn onClick={this.colorSelectedLegendeWhite} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/bleu.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedLegendeBlue}/>
                      <p id="titre">Teinte spéciale Bleu Alpine</p>
                      <p>1800€</p>
                      <MDBBtn onClick={this.colorSelectedLegendeBlue} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/noir.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedLegendeBlack}/>
                      <p id="titre">Teinte métallisée Noir Profont</p>
                      <p>840€</p>
                      <MDBBtn onClick={this.colorSelectedLegendeBlack} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </div>


{/* choix des jantes</MDBBtn> */}
          <div className="text-center">
          <h2>Jantes Légende</h2>
          </div>  

          {(this.state.jante === "" || this.state.jante === "legende ")  &&
            <div>
              
              <MDBContainer className="jante">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3">              
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg")} className="img-fluid choixJ"
                          alt="" />
                      <MDBBtn color="blue-grey" size="sm">Jante légende</MDBBtn>
                      <p>0€</p>
                    </MDBView>
                  </MDBCol>
                  
                  {this.state.color === "White" &&     
                  <MDBCol lg="9" className="mb-8 jante">
                    <img src={require("./assets/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg")} className="img-fluid"
                        alt=""/>
                  </MDBCol>
                  }

                  {this.state.color ==="Blue" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-bleu_jante-legende (3).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>
                  }
                  
                  {this.state.color ==="Black" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-noir_jante-legende (1).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>
                  
                   }            
                  
                </MDBRow>
              </MDBContainer>           
            </div> 
            }


{/* choix de l'interieur */}
          <div className="text-center">
          <h2>choix de l'interieur</h2>
          </div>  

          {this.state.interieur === "" || this.state.interieur === "Brun" &&
          
              <MDBContainer className="interieur">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3 int">              
                       <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-brun.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedLegendeBrun}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedLegendeBrun} size="sm">Cuir brun</MDBBtn>
                      <p>800€</p>
                    </MDBView>
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-noir.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedLegendeNoir}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedLegendeNoir} size="sm">Cuir noir</MDBBtn>
                      <p>0€</p>
                    </MDBView>
                  </MDBCol>


                  <MDBCol lg="9" className="mb-8 int1">
                  <ConfigCarousel withButton={false} path="interieurs/vues" slides={[
                    {
                      img: "cuir-brun-1.jpg",
                    },
                    {
                      img: "cuir-brun-2.jpg",
                    },
                    {
                      img: "cuir-brun-3.jpg",
                    }
                  ]} />   
                  </MDBCol>

                </MDBRow>
              </MDBContainer>           
            
          }



          {this.state.interieur === ""  || this.state.interieur === "Noir" &&
          
              <MDBContainer className="interieur">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3 int">              
                       <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-brun.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedLegendeBrun}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedLegendeBrun} size="sm">Cuir brun</MDBBtn>
                      <p>800€</p>
                    </MDBView>
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/interieurs/selection/cuir-noir.jpg")} className="img-fluid choixJ"
                          alt="" onClick={this.interieurSelectedLegendeNoir}/>
                      <MDBBtn color="blue-grey" onClick={this.interieurSelectedLegendeNoir} size="sm">Cuir noir</MDBBtn>
                      <p>0€</p>
                    </MDBView>
                  </MDBCol>


                  <MDBCol lg="9" className="mb-8 int1">
                  <ConfigCarousel withButton={false} path="interieurs/vues" slides={[
                    {
                      img: "cuir-noir-1.jpg",
                    },
                    {
                      img: "cuir-noir-2.jpg",
                    },
                    {
                      img: "cuir-noir-3.jpg",
                    }
                  ]} />   
                  </MDBCol>

                </MDBRow>
              </MDBContainer>           
             
          }
{/* choix des équipements */}
          <div>
            <h2 className="">Choix des équipements</h2>
          </div>
          <MDBContainer className="equipement">
            <h3>Design</h3>
            <MDBRow>
              <MDBCol lg="2" md="6" >
                <img src={require('./assets/configurateur/equipements/categories/design/pack-heritage.jpg')} className='img-fluid img-thumbnail '/>
                <p>Pack héritage</p>
                <p>180€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2" md="6">
              <img src={require('./assets/configurateur/equipements/categories/design/repose-pied-alu.jpg')} className='img-fluid img-thumbnail'/>
                <p>Repose-pieds passager en aluminium</p>
                <p>96€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
            </MDBRow>
            <h3>Média et navigation</h3>
            <MDBRow>
              <MDBCol lg="2" md="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/alpine-metrics.jpg')} className='img-fluid img-thumbnail'/>
                <p>Alpine Télémétrics</p>
                <p>204€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/audio-focal.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système Audio Focal</p>
                <p>600€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/audio-premium.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système Audio Focal Premium</p>
                <p>1200€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
              <MDBCol lg="2">
              <img src={require('./assets/configurateur/equipements/categories/media et navigation/audio-standard.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système Audio standard</p>
                <p>0€</p>
                <input filled type="checkbox" id="checkbox1" />
              </MDBCol>
            </MDBRow>
            <h3>Confort</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/pack-rangement.jpg')} className='img-fluid img-thumbnail'/>
                <p>Pack de rangement</p>
                <p>504€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/regul-limit-vitesse.jpg')} className='img-fluid img-thumbnail'/>
                <p>Régulateur / limiteur de vitesse</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/retro-ext-chaffant.jpg')} className='img-fluid img-thumbnail'/>
                <p>Rétroviseurs ext. chauf. rabattables élec.</p>
                <p>504€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/confort/retro-int-electrochrome.jpg')} className='img-fluid img-thumbnail'/>
                <p>Retroviseur intérieur électrochrome</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Conduite</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/aide-stationnement-ar.jpg')} className='img-fluid img-thumbnail'/>
                <p>Aide au stationnement AR</p>
                <p>420€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/aide-stationnement-av-ar.jpg')} className='img-fluid img-thumbnail'/>
                <p>Aide au stationnement AV et AR</p>
                <p>750€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/camera-recul.jpg')} className='img-fluid img-thumbnail'/>
                <p>Aide au stationnement AV AR et caméra de recul</p>
                <p>1200€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/conduite/echappement-sport.jpg')} className='img-fluid img-thumbnail'/>
                <p>Echappement Sport actif</p>
                <p>1500€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Sécurité</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/securite/aide-freinage-durgence.jpg')} className='img-fluid img-thumbnail'/>
                <p>Assistance au freinage d'urgence</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/securite/airbag.jpg')} className='img-fluid img-thumbnail'/>
                <p>Airbags frontaux conducteur et passager</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/securite/freinage-haute-perf.jpg')} className='img-fluid img-thumbnail'/>
                <p>Système de freinage Haute-Perf 320mm</p>
                <p>1000€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Perso. extérieur</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation exterieure/etrier-bleu.jpg')} className='img-fluid img-thumbnail'/>
                <p>Etriers de frein couleur Bleu Alpine</p>
                <p>384€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation exterieure/etrier-gris.jpg')} className='img-fluid img-thumbnail'/>
                <p>Etriers de frein couleur Gris Anthracite</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation exterieure/logo-alpine.jpg')} className='img-fluid img-thumbnail'/>
                <p>Logo Alpine sur les ailes avant</p>
                <p>120€</p>
                <input filled type="checkbox"/>
              </MDBCol>
            </MDBRow>
            <h3>Perso. intérieur</h3>
            <MDBRow>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/logo-volant.jpg')} className='img-fluid img-thumbnail'/>
                <p>Logo au centre du volant en Bleu Alpine</p>
                <p>84€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/pack-carbone.jpg')} className='img-fluid img-thumbnail'/>
                <p>Harmonie carbone mat</p>
                <p>0€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/pedal-alu.jpg')} className='img-fluid img-thumbnail'/>
                <p>Pédalier en aluminium</p>
                <p>120€</p>
                <input filled type="checkbox"/>
              </MDBCol>
              <MDBCol lg="2" className="">
              <img src={require('./assets/configurateur/equipements/categories/personnalisation interieure/siege-chauffant.jpg')} className='img-fluid img-thumbnail'/>
                <p>Sièges chauffants</p>
                <p>400€</p>
                <input filled type="checkbox"/>
              </MDBCol> 
            </MDBRow>

          </MDBContainer>

          </div>
        }

      </div>
    );
  }
}

export default App;
